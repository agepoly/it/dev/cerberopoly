#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (C) Maximilien Cuony 2013

from __future__ import print_function

import smartcard

from smartcard.CardMonitoring import CardMonitor, CardObserver
from smartcard.CardRequest import CardRequest

import argparse
import os
import re
import readline
import requests
import signal
import sqlite3


class Database:
    def __init__(self, name="db.sqlite3"):
        self.con = sqlite3.connect(name, check_same_thread=False)
        if not self.initialized:
            self.con.execute("CREATE TABLE people (email text, nb_entries int)")
            self.con.commit()

    @property
    def initialized(self):
        return self.con.execute("SELECT COUNT(name) AS c FROM sqlite_master WHERE type='table' AND name='people'").fetchone()[0] > 0

    def entry_sciper(self, sciper):
        response = requests.get("https://websrv.epfl.ch/rwspersons/getPerson?app=accred&id={}".format(sciper))
        try:
            email = response.json()["result"]["email"]
        except Exception:
            print("\x1b[31mCan't get email. This is probably not a valid SCIPER.\x1b[39m")
            return

        print("Email: {}".format(email))
        self.entry_mail(email)

    def entry_mail(self, email):
        cur = self.con.execute("SELECT nb_entries FROM people WHERE email=?", (email,))
        result = cur.fetchone()
        cur.close()
        if result is not None:
            nb_entries = result[0]
            self.con.execute("UPDATE people SET nb_entries = ? WHERE email = ?", (nb_entries+1, email))
            self.con.commit()
            if nb_entries == 0:
                print("\x1b[32mOK\x1b[39m")
            else:
                print("\x1b[33mWARNING: This person already entered {} times\x1b[39m".format(nb_entries))
        else:
            print("\x1b[31mNOT REGISTERED\x1b[39m")

    def load_file(self, filename):
        with open(filename, 'r') as f:
            for line in f:
                line = line[:-1] # Remove \n
                cur = self.con.execute("SELECT COUNT(email) as c FROM people WHERE email = ?", (line,))
                result = cur.fetchone()
                cur.close()
                if result[0] == 0:
                    self.con.execute("INSERT INTO people(email, nb_entries) VALUES (?, 0)", (line,))
                    self.con.commit()
                else:
                    print("Skipping address {} which is already present".format(line))


    def truncate(self):
        self.con.execute("DELETE FROM people")
        self.con.commit()


class CardWatch(CardObserver):

    def __init__(self, db):
        super(CardWatch, self).__init__()
        self.db = db

    def update(self, observable, changedcards):

        (addedcards, removedcards) = changedcards

        for card in addedcards:

            cardrequest = CardRequest(timeout=1, cardType=None)
            cardservice = cardrequest.waitforcard()
            cardservice.connection.connect()
            data, sw1, sw2 = cardservice.connection.transmit([255, 202, 0, 0, 0], protocol=smartcard.scard.SCARD_PCI_T1)
            card_id = data[:]
            data.reverse()
            CardId = ''.join(['%02X' % x for x in data])
            print('Read card {}'.format(CardId))

            response = requests.get("https://websrv.epfl.ch/cgi-bin/rwscamipro/getSciper?caller=288467&app=accred&Camipro={}".format(CardId))

            try:
                sciper = response.json()["Sciper"]
            except Exception:
                print("\x1b[31mCan't get SCIPER. Is this really a camipro ?\x1b[39m")
                return

            print("SCIPER: {}".format(sciper))
            self.db.entry_sciper(sciper)

            print("")
            os.kill(os.getpid(), signal.SIGWINCH) # Reprint prompt and line buffer


def get_argument_parser():
    """Add the list of argument to an argparse.ArgumentParser, return the parser"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", help="Alternative database file (default to db.sqlite3)")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-a", "--append", help="File of emails to add to the database")
    group.add_argument("-r", "--replace", help="File of emails to replace the database with")

    return parser


if __name__ == '__main__':
    parser = get_argument_parser()
    args = parser.parse_args()

    print("")
    print("")
    print("CerberoPoly V 1.0")
    print("")
    print("")

    db = Database(args.database) if args.database else Database()
    if args.append:
        db.load_file(args.append)
    if args.replace:
        db.truncate()
        db.load_file(args.replace)
    cardmonitor = CardMonitor()
    cardobserver = CardWatch(db)
    cardmonitor.addObserver(cardobserver)

    print("")
    print('Card reader is ready !')
    print('')
    print('')

    print('Scan card or enter SCIPER or email manually. "q" to quit.')

    readline.parse_and_bind("tab: complete")
    re_sciper = re.compile(r"^\b\d{6}\b$")
    re_mail = re.compile(r'^\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b$')

    while True:
        try:
            line = input("cerberopoly> ")
        except EOFError:
            break
        if re_sciper.match(line):
            db.entry_sciper(int(line))
        elif re_mail.match(line):
            db.entry_mail(line.strip())
        elif line in ['q', 'quit', 'exit']:
            break
        elif line:
            print("Unknown command")
        print("")

    print("")
    print("Bye")
    cardmonitor.deleteObserver(cardobserver)
