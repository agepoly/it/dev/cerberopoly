# CerberoPoly
This is a script to do an access control with a Camipro reader

## Setup
You need to be on the EPFL network (either on-site or VPN) for the API to be available.
This script needs python3 and a few dependencies. This depends on the system.

### On Ubuntu/Debian
Install the following packages with `apt`:

* `python3` (duh)
* `pcscd`
* `libacsccid1`
* `libpcsclite-dev` (needed to compile a python module later)
* `python3-dev` (same)
* `swig` (same)

Then install the following python3 modules with `pip`:

* `requests`
* `pyscard` (this is the package that needs 3 dependencies above)

Then start the daemon : `# systemctl start pcscd`.

### On Archlinux
Install the following packages with `pacman`:

* `python` (duh)
* `opensc` (this provides the `pcscd` daemon)
* `acsccid`
* `swig` (needed to compile a python module later)

Then install the following python3 modules with `pip`:

* `requests`
* `pyscard` (this is the package that needs a dependency above)

Then start the daemon : `# systemctl start pcscd`.

### Attention notice for all systems

If you use the *ACS ACR122U* card reader that PolyLAN and AGEPoly have, you'll need to unload or blacklist the `pn533` and `pn533_usb` kernel modules before starting the `pcscd` daemon. (Otherwise the daemon won't be able to use the card reader).

If you don't blacklist the modules, *every time you plug in the card reader*, the modules will be reloaded. In that case, unload them with `# rmmod pn533_usb` and `# rmmod pn533`, and also restart the daemon : `# systemctl restart pcscd`. Do not plug in or out the card reader !

Or, you can also blacklist the module, to avoid auto-reloading. [See this](https://www.networkworld.com/article/3270624/blacklisting-modules-on-linux.html). TLDR: edit the `/etc/modprobe.d/blacklist.conf` and add a line `blacklist pn533_usb` and another line `blacklist pn533`. Do add a comment to remember why (comments start with `#`).

## Configuration

You'll need a file with the epfl mail addresses of the authorized people, one per line. It will then be imported in a sqlite database to remember who has already passed the access control. Note that it is also possible to use non-epfl emails, but you will need to type them explicitely (camipro or SCIPER won't work).

To replace the current database with the file `filename`, add the `[-r|--replace] filename` option. Note that it will reset the passage count.

To add the emails in `filename` to the database, add the `[-a|--append] filename` option. The passage count will not be reset for emails already in the database. Note that this option is not compatible with `-r`.

The default database file is `db.sqlite3` in the current directory. You can override this with the option `[-d|--database] filename`.

## Usage

Execute the `cerberopoly.py` script (with eventual options, see above). You'll get a `cerberopoly> ` prompt. At this prompt, you can either:
- Scan a Camipro card
- Type a SCIPER number
- Type an email address
- Type "q", "quit", "exit" or "^D" to exit


You'll get a result of
- "OK" if the person is in the database and has not yet passed the access control
- "WARNING" if the person is in the database but has already passed the access control (with an access count)
- "ERROR" if the person is not in the database or can't be found (e.g. invalid Camipro or SCIPER)

## TODO
There are many possible improvements for this script:
- Allow to use SCIPERS instead of emails in the db
- Auto-complete the email addresses
- Allow to use multiple access points with only one database
- Add a GUI
- ...
